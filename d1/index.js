
// MATCH

	db.fruits.aggregate([
			{$match: {onSale: true}}
		]);

	db.fruits.aggregate([
			{$match: { stock: { $gte: 20} } }
		]);

// GROUP

	db.fruits.aggregate([
			{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } }
		]);


// SORT -1 decending, 1 ascending

	db.fruits.aggregate([
		{ $match: { onSale: true} },
		{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
		{ $sort: {total: -1} }
	]);

// PROJECT $project

	db.fruits.aggregate([
		{ $match: { onSale: true} },
		{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
		{ $project: {_id: 0} }
	]);

		db.fruits.aggregate([
		{ $match: { onSale: true} },
		{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
		{ $project: {_id: 0} },
		{ $sort: {total: -1 }}
	]);


// OPERATORS

// SUM - $sum
	
	db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } }
		])

		db.fruits.aggregate([
		{ $group: { _id: null, total: {$sum: "$stock"} } }
		])


//  MAX - $max

	db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", max_price: {$max: "$price"} } }
		])

	db.fruits.aggregate([
		{ $group: { _id: null, max_price: {$max: "$price"} } }
		])


// MIN - $min

	db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", min_price: {$min: "$price"} } }
		])

		db.fruits.aggregate([
		{ $group: { _id: null, min_price: {$min: "$price"} } }
		])


//  AVERAGE - $avg

	db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", avg_price: {$avg: "$price"} } }
		])

		db.fruits.aggregate([
		{ $group: { _id: null, avg_price: {$avg: "$price"} } }
		])