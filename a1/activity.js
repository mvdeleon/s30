

db.fruits.aggregate( [
   { $match: {onSale: true}},
   { $group: { _id: null, fuitsOnSale: { $sum: 1 } } },
   { $project: { _id: 0 } }
] )



db.fruits.aggregate( [
   { $match: { stock: { $gte: 20} } },
   { $group: { _id: null, enoughStock: { $sum: 1 } } },
   { $project: { _id: 0 } }
] )



db.fruits.aggregate([
	{ $match: {onSale: true}},
	{ $group: { _id: "$supplier_id", avg_price: {$avg: "$price"} } },
	{ $sort: {total: -1 }}
])


db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", max_price: {$max: "$price"} } }
])



db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", min_price: {$min: "$price"} } }
])